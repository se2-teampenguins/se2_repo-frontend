﻿using System.ComponentModel.DataAnnotations;

namespace Penguins_Front_End.Models.ViewModels
{
    /// <summary>
    /// UserList View Model
    /// </summary>
    public class UserListVM
    {
        public string Email { get; set; }
        public string UserName { get; set; }
        [Display(Name = "Number of Roles")]
        public int NumberOfRoles { get; set; }
        [Display(Name = "Role Names")]
        public string RoleNames { get; set; }
    }
}
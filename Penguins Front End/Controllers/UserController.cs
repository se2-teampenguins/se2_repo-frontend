﻿using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Penguins_Front_End.Models.ViewModels;
using Penguins_Front_End.Services;

namespace Penguins_Front_End.Controllers
{
    [Authorize(Roles="Admin")]
    public class UserController : Controller
    {
        private readonly IUserRepository _userRepo; //Injected the UserRepository
        private readonly IRoleRepository _roleRepo; //Injected the RoleRepository

        public UserController(IUserRepository userRepo, IRoleRepository roleRepo)
        {
            _userRepo = userRepo;
            _roleRepo = roleRepo;
        }

        /// <summary>
        /// Index action method
        /// </summary>
        /// <returns></returns>
        public async Task<IActionResult> Index()
        {
            var users = await _userRepo.ReadAllAsync();
            var userList = users
                .Select(u => new UserListVM
                {
                    Email = u.Email,
                    UserName = u.UserName,
                    NumberOfRoles = u.Roles.Count,
                    RoleNames = string.Join(",", u.Roles.ToArray())
                });
            return View(userList);
        }

        public IActionResult Data()
        {
            return View();
        }

        /// <summary>
        /// Assigns role action method
        /// </summary>
        /// <returns></returns>
        public async Task<IActionResult> AssignRoles()
        {
            var users = await _userRepo.ReadAllAsync(); //Read all users into users asynchronously
            var roles = _roleRepo.ReadAll(); //Read all roles into roles
            var model = new AssignRoleVM(); //Instantiate model as an AssignRoleVM
            model.UserNames = users.Select(u => u.UserName).ToList(); //Select UserName from users converted to a list
            model.RoleNames = roles.Select(r => r.Name).ToList(); //Select Name from roles converted to a list
            return View(model); //Return the view with the model as parameter
        }

        /// <summary>
        /// AssignRoles action Post method.
        /// </summary>
        /// <param name="roleVM">The role vm.</param>
        /// <returns></returns>
        [HttpPost, ValidateAntiForgeryToken]
        public async Task<IActionResult> AssignRoles(AssignRoleVM roleVM)
        {
            await _userRepo.AssignRoleAsync(roleVM.UserName, roleVM.RoleName); //Call user repository's AssignRoleAsync
            return RedirectToAction("Index", "User"); //Redirect to User Index
        }
    }
}
